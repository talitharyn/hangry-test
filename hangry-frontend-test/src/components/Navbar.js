import React, { useState } from "react";
import Logo from "../assets/Logo.svg";
import "../App.css";
const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <nav className="bg-white fixed w-full z-20 top-0 start-0 drop-shadow-md">
        <div className="pl-6 lg:pl-20 pr-4 max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-2 lg:p-3">
          <a
            href="/"
            className="flex items-center space-x-4 rtl:space-x-reverse"
          >
            <img src={Logo} alt="Hangry Logo" className="logo" />
          </a>
          <button
            onClick={() => setIsOpen(!isOpen)}
            data-collapse-toggle="navbar-default"
            type="button"
            className=" items-center p-2 w-10 h-10 justify-center lg:hidden"
            aria-controls="navbar-default"
            aria-expanded="false"
          >
            <span className="sr-only">Open main menu</span>
            <svg
              className="w-5 h-5"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 17 14"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M1 1h15M1 7h15M1 13h15"
              />
            </svg>
          </button>
          <div
            className={`items-center justify-between ${
              isOpen ? "block" : "hidden"
            } w-full lg:flex lg:w-auto lg:order-1`}
            id="navbar-sticky"
          >
            <ul className="flex flex-col pt-6 lg:p-4 font-medium space-y-2 lg:flex-row lg:space-x-12 lg:space-y-0 lg:items-center">
              <li className="relative">
                <a
                  href="/"
                  className="block py-2 lg:px-3 text-gray-900 rounded"
                >
                  Tentang Kami
                </a>
              </li>
              <li className="relative">
                <a href="/" className="block py-2 lg:px-3 text-gray-900">
                  Semua Outlet
                </a>
              </li>
              <li className="relative">
                <a href="/" className="block py-2 lg:px-3 text-gray-900">
                  Promo
                </a>
              </li>
              <li className="relative">
                <a href="/" className="block py-2 lg:px-3 text-gray-900">
                  Karier
                </a>
              </li>
              <li className="relative">
                <a
                  href="/"
                  className="block py-2 lg:px-3 text-rose-800 font-bold"
                >
                  Download App
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};
export default Navbar;
