import React from "react";
import iPhone from "../assets/iPhone-1.svg";
import iPhone2 from "../assets/iPhone-2.svg";
import AyamKoplo from "../assets/Ayam Koplo.svg";
import Sangyu from "../assets/Sangyu.svg";
import BudeSari from "../assets/Bude Sari.svg";
import KopiDariPada from "../assets/Kopi Dari|Pada.svg";

const Home = () => {
  const menuItems = [
    {
      name: "Ayam Koplo",
      description: "Ayam Geprek · Indonesia",
      image: AyamKoplo,
    },
    {
      name: "San Gyu",
      description: "Beef Bowl · Jepang",
      image: Sangyu,
    },
    {
      name: "Bude Sari",
      description: "Nasi Ayam · Indonesia",
      image: BudeSari,
    },
    {
      name: "Kopi Dari|Pada",
      description: "Minuman · Indonesia ",
      image: KopiDariPada,
    },
  ];
  return (
    <>
      <section
        id="home"
        className="w-full pt-24 lg:pt-36 bg-stone-50 flex flex-col lg: flex-row items-center justify-center"
      >
        <div className="container h-fit">
          <div className="pl-6 lg:pl-20 flex flex-wrap items-center lg:space-x-36 h-fit">
            <div className="h-40 mb-20 w-48 lg:w-fit lg:mb-48 self-center">
              <h2 className="text-neutral-600 text-[12px] font-bold lg:mb-3 lg:text-2xl text-justify">
                Kamu laper atau haus?
              </h2>
              <h1
                className="block font-bold text-dark w-[170px] text-2xl lg:text-5xl lg:w-[500px]"
                style={{ lineHeight: "1.5" }}
              >
                Tenang... ada Hangry! yang siap mengatasi
              </h1>
              <div className="flex pt-4 lg:w-full lg:space-x-4 lg:pt-12">
                <a
                  href="https://play.google.com/store/apps/details?id=com.modular.ishangry"
                  target="_blank"
                  rel="noopener noreferrer"
                  link="noopener"
                >
                  <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/2560px-Google_Play_Store_badge_EN.svg.png"
                    alt="google-play-badge"
                    className="h-16 hidden lg:block"
                  />
                </a>
                <a
                  href="https://apps.apple.com/us/app/hangry/id1498223490"
                  target="_blank"
                  rel="noopener noreferrer"
                  link="noopener"
                >
                  <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/800px-Download_on_the_App_Store_Badge.svg.png"
                    alt="app-store-badge"
                    className="lg:h-16 h-8"
                  />
                </a>
              </div>
            </div>
            <div className="ml-[42px] lg:w-fit lg:w-1/2 self-end lg:px-4">
              <div className="relative lg:mt-10 lg:mt-0 lg:right-0">
                <img
                  src={iPhone}
                  alt="Hangry App"
                  className="hidden lg:block max-w-full mx-auto hover:scale-105 transition duration-300 ease-in-out"
                />
              </div>
              <div>
                <img
                  src={iPhone2}
                  alt="Hangry App-2"
                  className="lg:hidden h-64"
                ></img>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section
        id="Outlet"
        className="pt-10 lg:pt-14 bg-white flex items-center justify-center"
      >
        <div className="container h-fit w-fit">
          <div className="pl-6 lg:pl-8 items-center h-fit">
            <h1 className="block font-bold text-2xl lg:text-5xl">
              Hangry! adalah restoran dengan beragam brand
            </h1>
            <h1 className="mt-2 lg:mt-4 text-gray-500 text-lg">
              Kami menyajikan beragam brand untuk menemani setiap waktu santapmu
            </h1>
          </div>
          <div className="pl-[24px] pr-[24px] lg:pl-8 mt-4 lg:mt-10 lg:pr-0 grid grid-cols-2 lg:grid-cols-3 gap-4 lg:gap-10">
            {menuItems.map(
              (item) =>
                (item.name !== "Kopi Dari|Pada" ||
                  (item.name === "Kopi Dari|Pada" &&
                    window.innerWidth < 1024)) && (
                  <div
                    key={item.name}
                    className="flex flex-col items-start text-left"
                  >
                    <img
                      src={item.image}
                      style={{
                        borderRadius: "16px",
                        boxShadow: "8px 4px 23px -5px rgba(0,0,0,0.35)",
                      }}
                      className="h-fit"
                      alt={item.name}
                    />
                    <div className="mt-2 lg:mt-6">
                      <h3 className="text-md font-bold lg:text-[32px] flex items-center">
                        {item.name}
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="ml-2 h-5 w-5 lg:h-8 lg:w-8 text-[#A30926]"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={3}
                            d="M14 5l7 7m0 0l-7 7m7-7H3"
                          />
                        </svg>
                      </h3>
                      <p className="mt-[1.2px] text-xs lg:mt-2 lg:text-2xl text-[#5A5A5A]">
                        {item.description}
                      </p>
                    </div>
                  </div>
                )
            )}{" "}
          </div>
        </div>
      </section>
      <section
        id="Find"
        className="pt-10 lg:pt-24 flex flex-col bg-white items-center justify-center"
      >
        <div className="container mx-auto text-center">
          <h1 className="text-[10px] text-zinc-600 lg:text-xl">
            Hangry! dapat kamu temukan di:
          </h1>
          <div className="w-full">
            <div className="flex flex-wrap items-center justify-center">
              <a
                href="#"
                className="mr-[20px] lg:mx-10 lg:py-4 grayscale opacity-80 transition duration-500 hover:grayscale-0 hover:opacity-100 hover:scale-105"
              >
                <img
                  src="https://1000logos.net/wp-content/uploads/2023/01/Gofood-logo.png"
                  alt="GoFood"
                  className="h-10 w-auto h-10 lg:h-28"
                />
              </a>
              <a
                href="#"
                className="mr-[20px] lg:mx-10 lg:py-4 grayscale opacity-80 transition duration-500 hover:grayscale-0 hover:opacity-100 hover:scale-105"
              >
                <img
                  src="https://www.pilonacoffee.com/wp-content/uploads/2022/03/logo-grabfood.png"
                  alt="GrabFood"
                  className="h-[14px] w-auto h-10 lg:h-10"
                />
              </a>
              <a
                href="#"
                className="mr-[20px] lg:mx-10 lg:py-4 grayscale opacity-80 transition duration-500 hover:grayscale-0 hover:opacity-100 hover:scale-105"
              >
                <img
                  src="https://upload.wikimedia.org/wikipedia/commons/0/09/Zomato_company_logo.png"
                  alt="Zomato"
                  className="h-4 w-auto h-10 lg:h-12"
                />
              </a>
              <a
                href="#"
                className="lg:mx-10 lg:py-4 grayscale opacity-80 transition duration-500 hover:grayscale-0 hover:opacity-100 hover:scale-105"
              >
                <img
                  src="https://yasinyasintha.com/wp-content/uploads/2020/03/Eats.png"
                  alt="EatsbyTraveloka"
                  className="h-4 w-auto h-10 lg:h-20"
                />
              </a>
            </div>
          </div>
        </div>
      </section>
      <section
        id="Order"
        className="pt-8 pb-10 w-full lg:pt-14 flex flex-col lg: flex-row items-center justify-center"
      >
        <div className="container h-fit">
          <div className="pl-6 lg:pl-20 flex flex-col-reverse lg:flex-row lg:items-center lg:h-fit lg:space-x-16">
            <div className="flex flex-col lg:w-1/2">
              <div className="text-[#303030] text-2xl lg:text-5xl font-bold">
                Makin rame, makin hemat!
              </div>
              <div className="mt-2 text-[#5A5A5A] lg:text-2xl lg:mt-4">
                Nikmati keuntungan dengan minimum <br /> pembelian 20 porsi menu
                apa aja. Cocok untuk hidangan segala acara!
              </div>
              <div className="mt-6 flex items-center lg:mt-6">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="fill-[#A30926] mr-4 lg:mr-8 w-[20px] h-[20px] lg:w-[40px] lg:h-[40px]"
                  viewBox="0 0 24 24"
                >
                  <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                </svg>
                <span className="text-[#303030] text-[18px] font-bold lg:text-3xl">
                  Beragam pilihan menu
                </span>
              </div>
              <div className="mt-6 flex items-center lg:mt-6">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="fill-[#A30926] mr-4 lg:mr-8 w-[20px] h-[20px] lg:w-[40px] lg:h-[40px]"
                  viewBox="0 0 24 24"
                >
                  <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                </svg>
                <span className="text-[#303030] text-[18px] font-bold lg:text-3xl">
                  Semua menu diskon 20%
                </span>
              </div>
              <div className="mt-6 flex items-center lg:mt-6">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="fill-[#A30926] mr-4 lg:mr-8 w-[20px] h-[20px] lg:w-[40px] lg:h-[40px]"
                  viewBox="0 0 24 24"
                >
                  <path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" />
                </svg>
                <span className="text-[#303030] text-[18px] font-bold lg:text-3xl">
                  Gratis biaya kirim
                </span>
              </div>
              <div className="flex items-center text-[16px] lg:text-[20px] lg:mt-1 text-[#5A5A5A] ml-[36px] lg:ml-[72px]">
                <h1>*Syarat & ketentuan berlaku</h1>
              </div>
              <div className="mr-8 mt-6 lg:mt-12">
                <button
                  type="button"
                  className="px-[95px] py-[14px] lg:px-[32px] lg:py-[20px] font-bold text-white text-[14px] lg:text-[24px] bg-[#A30926] hover:bg-[#851026] rounded-lg"
                >
                  Pesan Sekarang
                </button>
              </div>
            </div>
            <div>
              <div className="rounded-xl w-full mb-4">
                <img
                  className="w-[310px] lg:w-[500px] rounded-xl"
                  src="https://cdn6.ep.dynamics.net/s3/rw-propertyimages/7175-S1849893-hires.25247-unnamed.jpg"
                  alt="Rame"
                ></img>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
